<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MWV5NLW');</script>

  <!-- mailchimp -->
  <script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us12.list-manage.com","uuid":"7818405885cdfbce09b1fc94b","lid":"089329eb4d","uniqueMethods":true}) })</script>

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css', false, null );

  wp_enqueue_style( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css', true, null );

  wp_enqueue_style( 'ionicons', 'https://unpkg.com/ionicons@4.5.9-1/dist/css/ionicons.min.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true );
  } 

  wp_enqueue_script( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array(), null, true );

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

    wp_enqueue_script( 'barba', 'https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js', array(), null, true );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );


// Image sizes
add_image_size('news', 768, 476, true);
add_image_size('products', 768, 1043, true);
add_image_size('gallery', 600, 350, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array( 
    
  ) );
}  

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' )   // main nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */


//accordion shortcode
function fold_shortcode( $atts , $content = null ) {

    $atts = shortcode_atts(
      array(
        'titel' => '',
      ),
      $atts
    );

  return '<div class="accordion__wrapper"><h3 class="accordion__title">' . $atts['titel'] . '<i class="icon ion-ios-arrow-down"></i></h3><div class="accordion__panel">' . $content . '</div></div>';

}

add_shortcode( 'fold', 'fold_shortcode' );


// Register Custom Post Type reference
function create_reference_cpt() {

  $labels = array(
    'name' => _x( 'Referencer', 'Post Type General Name', 'lionlab' ),
    'singular_name' => _x( 'Reference', 'Post Type Singular Name', 'lionlab' ),
    'menu_name' => _x( 'Referencer', 'Admin Menu text', 'lionlab' ),
    'name_admin_bar' => _x( 'Reference', 'Add New on Toolbar', 'lionlab' ),
    'archives' => __( 'Reference Archives', 'lionlab' ),
    'attributes' => __( 'reference Attributes', 'lionlab' ),
    'parent_item_colon' => __( 'Parent reference:', 'lionlab' ),
    'all_items' => __( 'Alle referencer', 'lionlab' ),
    'add_new_item' => __( 'Add New reference', 'lionlab' ),
    'add_new' => __( 'Tilføj ny', 'lionlab' ),
    'new_item' => __( 'Ny reference', 'lionlab' ),
    'edit_item' => __( 'Redigere reference', 'lionlab' ),
    'update_item' => __( 'Update reference', 'lionlab' ),
    'view_item' => __( 'View reference', 'lionlab' ),
    'view_items' => __( 'View references', 'lionlab' ),
    'search_items' => __( 'Search reference', 'lionlab' ),
    'not_found' => __( 'Not found', 'lionlab' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
    'featured_image' => __( 'Featured Image', 'lionlab' ),
    'set_featured_image' => __( 'Set featured image', 'lionlab' ),
    'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
    'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
    'insert_into_item' => __( 'Insert into reference', 'lionlab' ),
    'uploaded_to_this_item' => __( 'Uploaded to this reference', 'lionlab' ),
    'items_list' => __( 'references list', 'lionlab' ),
    'items_list_navigation' => __( 'references list navigation', 'lionlab' ),
    'filter_items_list' => __( 'Filter references list', 'lionlab' ),
  );
  $args = array(
    'label' => __( 'reference', 'lionlab' ),
    'description' => __( '', 'lionlab' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-heart',
    'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'trackbacks', 'page-attributes', 'post-formats', 'custom-fields'),
    'taxonomies' => array('category', 'referencer'),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'hierarchical' => false,
    'exclude_from_search' => false,
    'show_in_rest' => true,
    'publicly_queryable' => true,
    'capability_type' => 'post',
  );
  register_post_type( 'reference', $args );

}
add_action( 'init', 'create_reference_cpt', 0 );


/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>