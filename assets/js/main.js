jQuery(document).ready(function($) {

  //menu toggle
  function menuToggle() {
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
  }

  menuToggle();


  //disable ajax contact button
  function disableAjax () {
    $('.no-ajax a').addClass('no-ajax');
  }

  disableAjax();


  //owl slider/carousel
  function referencesSlider() {
    var owl = $('.references__track');

    owl.each(function() {
      $(this).children().length > 1;

        $(this).owlCarousel({
          loop: false,
          items: 3,
          StartPosition: 2, 
          autoplay: false,
          nav: true,
          autplaySpeed: 11000,
          autoplayTimeout: 10000,
          smartSpeed: 250,
          smartSpeed: 2200,
          navSpeed: 2200, 
          navText : ["<i class='icon ion-ios-arrow-round-back'></i>", "<i class='icon ion-ios-arrow-round-forward'></i>"],
          responsiveClass: true,
          responsive : {
          // breakpoint from 0 up
          0 : {
              items: 1,
              nav: true
          },
          // breakpoint from 480 up
          480 : {
              items: 2,
              nav: true
          },
          // breakpoint from 768 up
          768 : {
              items: 3,
              nav: true
          }
        }
        });
    });
  }

  referencesSlider();

  //owl slider/carousel
  function productSlider() {
    var owl = $('.slider__track');

    owl.each(function() {
      $(this).children().length > 1;

        $(this).owlCarousel({
          loop: true,
          items: 1,
          autoplay: false,
          nav: true,
          autplaySpeed: 11000,
          autoplayTimeout: 10000,
          smartSpeed: 250,
          smartSpeed: 2200,
          navSpeed: 2200, 
          navText : ["<i class='icon ion-ios-arrow-round-back'></i>", "<i class='icon ion-ios-arrow-round-forward'></i>"]
        });
    });
  }

  productSlider();


  //owl slider/carousel
  function slider() {
    var owl = $('.slider__list');

    owl.each(function() {
      $(this).children().length > 1;

        $(this).owlCarousel({
          loop: true,
          items: 1,
          autoplay: true,
          nav: false,
          autplaySpeed: 11000,
          autoplayTimeout: 10000,
          smartSpeed: 250,
          smartSpeed: 800,
          navSpeed: 2200
        });
    });
  }

  slider();


  //toggle accordion
  function accordion() {
    var $acc = $('.accordion__title'); 

      $acc.each(function() {
        $(this).click(function() {
          $(this).toggleClass('is-active');
          $(this).next().toggleClass('is-visible');
      });
    });
  }

  accordion();

  //wrap linkboxes
  function wrap() {
    var gallery = $('.link-boxes__item');

    for (var i = 0; i < gallery.length; i += 2) {
        gallery.slice(i, i + 2).wrapAll('<div class="link-boxes__wrapper col-sm-6" />');
    }
  }

  wrap(); 


  //update active class
  function activeClass() {
    $('.nav__item').not('.current-menu-ancestor, .is-dropdown').on('click', function(){
      $(".nav__item.is-active").removeClass("is-active"); 
      $(this).addClass('is-active'); 
    });
  }

  activeClass();
  

   //
  // Barba.js
  //
  Barba.Pjax.Dom.wrapperId = 'ajax-wrapper';
  Barba.Pjax.Dom.containerClass = 'ajax-container';
  Barba.Pjax.ignoreClassLink = 'no-ajax';

  var general = Barba.BaseView.extend({
    namespace: 'general',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
      
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
         $('.ajax-progress').removeClass('is-active');
        
    },
    onLeave: function() {
      // A new Transition toward a new page has just started.
       $('.ajax-progress').addClass('is-active');
      
    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.

    }
  });

  general.init();

  Barba.Pjax.start();

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
  /**
  * This function is automatically called as soon the Transition starts
  * this.newContainerLoading is a Promise for the loading of the new container
  * (Barba.js also comes with an handy Promise polyfill!)
  */
  // As soon the loading is finished and the old page is faded out, let's fade the new page
  Promise
  .all([this.newContainerLoading, this.fadeOut()])
  .then(this.fadeIn.bind(this));
  },
  fadeOut: function() {
  /**
  * this.oldContainer is the HTMLElement of the old Container
  */
  //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  return $(this.oldContainer).animate({ opacity: 1 }).promise();
  },
  fadeIn: function() {
    /**
    * this.newContainer is the HTMLElement of the new Container
    * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
    * Please note, newContainer is available just after newContainerLoading is resolved!
    */
    var _this = this;
    var $newContainer = $(this.newContainer);

    $(this.oldContainer).hide();
    $(window).scrollTop(0); // scroll to top here

    $newContainer.css({
      visibility : 'visible',
      opacity : 1,
      "margin-left" : "0px", 
    });

    $newContainer.animate({
      opacity: 1,
      "margin-left" : 0,
    }, 800, function() {
      /**
      * Do not forget to call .done() as soon your transition is finished!
      * .done() will automatically remove from the DOM the old Container
      */
      _this.done();

    });
    }

  });
  /**
  * Next step, you have to tell Barba to use the new Transition
  */
  Barba.Pjax.getTransition = function() {
  /**
  * Here you can use your own logic!
  * For example you can use different Transition based on the current page or link...
  */
  return FadeTransition;

  };

  Barba.Dispatcher.on('initStateChange', function() {

    menuToggle();
    accordion(); 
    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.nav__dropdown').removeClass('is-visible');

    //send page view on every page transition
    if (Barba.HistoryManager.history.length <= 1) {
        return;
    }

    if (typeof gtag === 'function') {
        // send statics by Google Analytics(gtag.js)
        gtag('config', 'GTM-MWV5NLW', {'page_path': location.pathname, 'use_amp_client_id': true});
    } 

    else {
        // send statics by Google Analytics(analytics.js) or Google Tag Manager
        if (typeof ga === 'function') {
            var trackers = ga.getAll();
            trackers.forEach(function (tracker) {
                ga(tracker.get('name') + '.send', 'pageview', location.pathname, {'useAmpClientId': true});
            });
        }
    }

  });

  /**
  * GET WP body classes
  */
  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );
    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses);

    referencesSlider();
    productSlider();
    slider();
    menuToggle();
    accordion(); 
    wrap();
    // triggerAcc();
    activeClass();

  });

  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {

  });

});
