<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'featured-products' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'featured-products' ); ?>

    <?php
    } elseif( get_row_layout() === 'flexible_columns' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'flexible_columns' ); ?>

    <?php
    } elseif( get_row_layout() === 'staggered_images_with_text' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'staggered_images_with_text' ); ?>

    <?php
    } elseif( get_row_layout() === 'slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'accordions' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'accordions' ); ?>

    <?php
    } elseif( get_row_layout() === 'latest-posts' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'latest-posts' ); ?>

    <?php
    } elseif( get_row_layout() === 'text-image-slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'text-img-slides' ); ?>

    <?php
    } elseif( get_row_layout() === 'references' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'references' ); ?>

    <?php
    } elseif( get_row_layout() === 'contact' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'contact' ); ?>

    <?php
    } elseif( get_row_layout() === 'product-overview' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'product-overview' ); ?>

    <?php
    } elseif( get_row_layout() === 'text-image' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'text-image' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
