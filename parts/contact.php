<?php 
/**
* Description: Lionlab contact field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


$text = get_field('contact_text', 'options');
$title = get_field('contact_title', 'options');

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

?>

<section class="contact <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row">

			<div class="col-sm-6 contact__intro">
				<h2 class="contact__title"><?php echo $title; ?></h2>
			</div>

			<div class="col-sm-6 contact__intro">
				<?php echo $text; ?>
			</div>
			
			<div class="col-sm-10 col-sm-offset-1">
				<?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
			</div>

		</div>
	</div>
</section>