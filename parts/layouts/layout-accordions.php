<?php 
/**
* Description: Lionlab accordions repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('title');

if (have_rows('accordion') ) :
?>

<section class="accordion <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-6">
				<h2 class="accordion__header"><?php echo $title; ?></h2>
			</div>

			<div class="col-sm-6">
				<?php while (have_rows('accordion') ) : the_row(); 
					$title = get_sub_field('title');
					$text = get_sub_field('text');
				?>

				<div class="accordion__wrapper">
					<h3 class="accordion__title"><?php echo esc_html($title); ?><i class="icon ion-ios-arrow-down"></i></h3>
					<div class="accordion__panel">
						<?php echo $text; ?>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>