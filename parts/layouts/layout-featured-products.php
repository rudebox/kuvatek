<?php 
/**
* Description: Lionlab featured products field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

if (have_rows('products') ) :

?>

<section class="featured-products <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-6 featured-products__intro">
				<h2 class="featured-products__header"><?php echo $title; ?></h2>
			</div>
			
			<div class="col-sm-6 featured-products__intro">
				<?php echo esc_html($text); ?>
			</div>

			<?php while (have_rows('products') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$img = get_sub_field('img');
				$mail = get_sub_field('mail');
				$link_text = get_sub_field('link_text');
				$subject = get_sub_field('email_subject');
			?>
			
			<div class="featured-products__item col-sm-4">
				<img src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<h3 class="featured-products__title"><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>

				<a class="btn btn--gradient featured-products__btn" href="mailto:<?php echo esc_html($mail); ?>?subject=<?php echo esc_html($subject); ?>"><?php echo esc_html($link_text); ?></a>
			</div>


			<?php endwhile; ?>
	
		</div>
	</div>
</section>
<?php endif; ?>