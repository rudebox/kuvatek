<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

if (have_rows('products') ) :

?>

<section class="product-overview <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-6 product-overview__intro">
				<h2 class="product-overview__header"><?php echo $title; ?></h2>
			</div>

			<div class="col-sm-6 product-overview__intro">
				<?php echo esc_html($text); ?>
			</div>

			<?php while (have_rows('products') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$img = get_sub_field('img');
				$link = get_sub_field('link');

			?>
			
			<a href="<?php echo esc_url($link); ?>" class="product-overview__item col-sm-4">
				<img src="<?php echo esc_url($img['sizes']['products']); ?>" alt="<?php echo esc_url($img['alt']); ?>">
				<h3 class="product-overview__title"><?php echo esc_html($title); ?></h3>
				<p><?php echo esc_html($text); ?></p>

				<span class="btn--text">Læs mere</span>
			</a>


			<?php endwhile; ?>
	
		</div>
	</div>
</section>
<?php endif; ?>