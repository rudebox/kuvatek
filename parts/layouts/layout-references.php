<?php 
/**
* Description: Lionlab references field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$header = get_sub_field('header');
$text = get_sub_field('text');

// Custom WP query query
$args_query = array(
  'posts_per_page' => 9,
  'post_type' =>  'reference',
  'order' => 'DESC',
  'cat' => 3
);

$query = new WP_Query( $args_query );

?>

<?php  if ($query->have_posts() ) : ?>

<section class="references <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-6 references__intro">
				<h2 class="references__header"><?php echo $header; ?></h2>
			</div>

			<div class="col-sm-6 references__intro">
				<?php echo esc_html($text); ?>
			</div>

			<div class="references__track is-slider">

				<?php while ($query->have_posts() ) : $query->the_post(); ?>
	        	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );?>

					<div class="references__item">
				        <div class="references__img" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
				
				        </div>

				        <div class="references__text">
				        	<h2 class="references__title"><?php the_title(); ?></h2>
				        	<em><?php the_excerpt(); ?></em>
				        	<a class="btn--text references__btn" href="<?php the_permalink(); ?>">Læs mere</a>
				        </div>
			        </div>

		    	<?php endwhile; ?>

		    	<?php wp_reset_postdata(); ?>

	    	</div>
		
		</div>
	</div>
</section>
<?php endif; ?>