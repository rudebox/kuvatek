<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

if (have_rows('linkbox') ) :

?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-6 link-boxes__intro">
				<h2 class="link-boxes__header"><?php echo $title; ?></h2>
			</div>

			<div class="col-sm-6 link-boxes__intro">
				<?php echo esc_html($text); ?>
			</div>

			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$img = get_sub_field('icon');
				$link = get_sub_field('link');

				$row = get_row_index();

			?>
			
			<a href="<?php echo esc_url($link); ?>" class="link-boxes__item link-boxes__item--<?php echo $row; ?>">
				<div class="link-boxes__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);">				
				</div>
				<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>
			</a>


			<?php endwhile; ?>
	
		</div>
	</div>
</section>
<?php endif; ?>