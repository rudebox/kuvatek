<?php 
/**
* Description: Lionlab text-image field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');


//field group
$title = get_sub_field('title');
$text = get_sub_field('text-image');
$img = get_sub_field('img');
$position = get_sub_field('position');

?>

<section class="text-image <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap text-image__row <?php echo esc_attr($position); ?>">

			<div class="col-sm-6 text-image__text <?php echo esc_attr($position); ?>">
				<h2 class="text-image__title"><?php echo $title; ?></h2>
				<?php echo $text; ?>
			</div>

			<div class="col-sm-6 text-image__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);"> 
				
			</div>
			
	
		</div>
	</div>
</section>