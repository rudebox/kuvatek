<?php 
/**
* Description: Lionlab text-image field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');


//field group
$title = get_sub_field('title');
$text = get_sub_field('text-image');
$position = get_sub_field('position');

if (have_rows('slides') ) :
?>
<section class="product <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap product__row <?php echo esc_attr($position); ?>">

			<div class="col-sm-6 product__text <?php echo esc_attr($position); ?>">
				<h2 class="product__title"><?php echo $title; ?></h2>
				<?php echo $text; ?>
			</div>
			
			<div class="slider__track is-slider is-slider--product product__track col-sm-6 <?php echo esc_attr($position); ?>">
				<?php 
					while (have_rows('slides') ) : the_row(); 

					$img = get_sub_field('slides_img');
				?>
				<div class="product__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);"> 
				</div>
				<?php endwhile; ?>
			</div>
			
		</div>
	</div>
</section>
<?php endif; ?>