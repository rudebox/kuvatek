<?php 
//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$header = get_sub_field('header');

//category field
$cat = get_sub_field('cat');

//fields
$link = get_sub_field('link');
$position = get_sub_field('position');


// Custom WP query query
$args_query = array(
  'posts_per_page' => 3,
  'order' => 'DESC',
  'cat' => $cat,
);

$query = new WP_Query( $args_query );


?>

<section class="archive <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
  <div class="wrap hpad">
    <h2 class="archive__header"><?php echo esc_html($header); ?></h2>
    <div class="row">

        <?php if ($query->have_posts()): ?>
        <?php while ($query->have_posts()): $query->the_post(); ?>

        <article class="archive__item col-sm-12" itemscope itemtype="http://schema.org/BlogPosting">
          
          <div class="archive__row row flex flex--wrap <?php echo esc_attr($position); ?>">
            <div class="archive__thumbnail col-sm-6">
               <a href="<?php the_permalink(); ?>">                 
                <?php the_post_thumbnail('news'); ?>
              </a>
            </div>

            <div class="col-sm-6" itemprop="articleBody">
              <h2 class="archive__title" itemprop="headline" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
              <?php the_excerpt(); ?>
              
              <p class="archive__cat">
               Kategorier: <?php the_category(', ') ?>              
              </p>

              <p><a class="btn--text" href="<?php the_permalink(); ?>">Læs mere</a></p>
            </div>
          </div>

        </article>

        <?php endwhile; else: ?>

        <p>No posts here.</p>

      <?php endif; ?>

      <?php wp_reset_postdata(); ?>
    </div>

    
    <p class="center"> 
      <a class="btn btn--gradient" href="<?php echo esc_url($link); ?>">Se alle indlæg</a>
    </p>

  </div>
</section>