<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//custom fields
	$img = get_field('page_img') ? : get_field('page_img', 'options');
	$text = get_field('page_text');

	//get thumb
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
?>

<div class="hero--wrapper">
	<section class="hero blue-light--bg padding--both">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">
				<div class="hero__text col-sm-6">
					<h1 class="hero__title"><?php echo $title; ?></h1>
					<?php echo $text; ?>
				</div>
				
				<?php if (is_single() ) : ?>
				<div class="hero__img col-sm-6" style="background-image: url(<?php echo $thumb[0]; ?>);"></div>
				<?php endif; ?>

				<?php if (!is_single() && (!is_front_page() ) )  : ?>
				<div class="hero__img col-sm-6" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>
				
				<?php endif; ?>

				<?php if (is_front_page() && have_rows('page_repeater') )  :  ?>
				
    			<div class="slider__list is-slider is-slider--home">

					<?php while (have_rows('page_repeater') ) : the_row(); 
						$slides = get_sub_field('page_slides');
					?>
			        <div class="slider__item" style="background-image: url(<?php echo esc_url($slides['url']); ?>);">
				   	</div>
				   	<?php endwhile; ?>

			   	</div>
				
				<?php endif; ?>
					

				<div class="hero__breadcrumbs flex">
					<?php if (is_front_page() ) : ?>
					 <div class="hero__scroll col-sm-6 flex flex--hvalign">
					 	<div class="hero__scroll--wrapper flex flex--hvalign">
				            <div class="hero__scroll--mousepad">
				              <span class="hero__scroll--mousepad--anim"></span>
				            </div>
				            <p class="hero__scroll--text">scroll</p>
			            </div>
		         	 </div>
					<?php endif; ?>	
					
					<?php if (!is_front_page() ) : ?>
						<div class="col-sm-6 hero__breadcrumbs--col flex flex--hvalign">
							<?php
								//breadcrumbs nom nom
								if ( function_exists('yoast_breadcrumb') ) {
								  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
								}
							?>
						</div>
					<?php endif; ?>	
				</div>
			</div>
		</div>
	</section>
</div>