<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

 <section class="archive padding--both">
  <div class="wrap hpad">
    <div class="row">

        <?php if (have_posts()): ?>
        <?php while (have_posts()): the_post(); ?>

        <article class="archive__item col-sm-12" itemscope itemtype="http://schema.org/BlogPosting">
          
          <div class="archive__row row flex flex--wrap">
            <div class="archive__thumbnail col-sm-6">
               <a href="<?php the_permalink(); ?>">                 
                <?php the_post_thumbnail('news'); ?>
              </a>
            </div>

            <div class="col-sm-6" itemprop="articleBody">
              <h2 class="archive__title" itemprop="headline" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
              <?php the_excerpt(); ?>

              <p><a class="btn--text" href="<?php the_permalink(); ?>">Læs mere</a></p>
            </div>
          </div>

        </article>

        <?php endwhile; else: ?>

        <p>No posts here.</p>

      <?php endif; ?>
    </div>
  </div>
</section>

<?php 
  $text = get_field('contact_text', 'options');
  $title = get_field('contact_title', 'options');
 ?>

<section class="contact blue-light--bg padding--both">
  <div class="wrap hpad">
    <div class="row">

      <div class="col-sm-6 contact__intro">
        <h2 class="contact__title"><?php echo $title; ?></h2>
      </div>

      <div class="col-sm-6 contact__intro">
        <?php echo $text; ?>
      </div>
      
      <div class="col-sm-10 col-sm-offset-1">
        <?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
      </div>

    </div>
  </div>
</section>

</main>

<?php get_template_part('parts/footer'); ?>