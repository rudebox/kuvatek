<?php
 /**
   * Description: Lionlab clean walker
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   * @author Original Author <https://gist.github.com/hereswhatidid/8904445>
   */

	class Clean_Walker_Nav extends Walker_Nav_Menu {

	function check_current($classes) {
		return preg_match('/(current[-_])|active|dropdown/', $classes);
	}

	function start_lvl(&$output, $depth = 0, $args = array()) {
		$output .= '<ul class="nav__dropdown">';
	}

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		$item_html = '';

		parent::start_el($item_html, $item, $depth, $args);

		$item_html = str_replace('<a', '<a class="nav__link"', $item_html);

		if ($item->is_dropdown && ($depth === 0)) {
			$item_html = str_replace('<a', '<a class="nav__link nav__dropdown-trigger"', $item_html);
			$item_html = str_replace('</a>', ' <i class="nav__link-icon"></i></a>', $item_html);
		}
		elseif (stristr($item_html, 'li class="divider')) {
			$item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
		}
		elseif (stristr($item_html, 'li class="dropdown-header')) {
			$item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
		}

		if(!empty($item->description)) {
			$item_html .= '<small class="nav__item--desc">' . esc_attr( $item->description ) . '</small>';
		}

		$item_html = apply_filters('wp_nav_menu_item', $item_html);
		$output .= $item_html;
	}

	function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
		$element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));

		if ($element->is_dropdown) {
			$element->classes[] = 'is-dropdown';
		}

		parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
	}
}



//Check if element is empty

function is_element_empty($element) {
	$element = trim($element);
	return !empty($element);
}



// Remove the id="" on nav menu items and add active class
// Return 'menu-slug' for nav menu classes

function nav_menu_css_class($classes, $item) {
	global $post;
	$active = 'is-active';

	$slug = sanitize_title($item->title);
	$classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', $active, $classes);
	$classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

	//Custom post type
	
	$id = ( isset($post->ID) ? get_the_ID() : null );

	// Check if post ID exists
	if ( isset($id) ) {
		$classes[] = ( $item->url == get_post_type_archive_link($post->post_type) ? $active : '' );
	}
	
	$classes[] = 'nav__item';
	// $classes[] = 'nav__item--' . $slug;

	$classes = array_unique($classes);

	return array_filter($classes, 'is_element_empty');
}

add_filter('nav_menu_css_class', 'nav_menu_css_class', 10, 2);

// Remove id
add_filter('nav_menu_item_id', '__return_null');

?>