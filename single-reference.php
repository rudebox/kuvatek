<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="single padding--both">
    <div class="wrap hpad">

      <article class="single__item" itemscope itemtype="http://schema.org/BlogPosting">

        <div itemprop="articleBody">
          <?php the_content(); ?>
          <a class="btn btn--gradient single__btn" onclick="window.history.go(-1); return false;">Tilbage</a>
        </div>

      </article>

    </div>
  </section>

  <?php get_template_part('parts/gallery'); ?>

  <?php 
    $text = get_field('contact_text', 'options');
    $title = get_field('contact_title', 'options');
  ?>

  <section class="contact blue-light--bg padding--both">
    <div class="wrap hpad">
      <div class="row">

        <div class="col-sm-6 contact__intro">
          <h2 class="contact__title"><?php echo $title; ?></h2>
        </div>

        <div class="col-sm-6 contact__intro">
          <?php echo $text; ?>
        </div>
        
        <div class="col-sm-10 col-sm-offset-1">
          <?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
        </div>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>